FROM docker.io/library/alpine:3.13.2@sha256:4661fb57f7890b9145907a1fe2555091d333ff3d28db86c3bb906f6a2be93c87 AS initrd

WORKDIR /initrd

# hadolint ignore=DL3018
RUN mkdir -p ./etc/apk \
    && cp -r /etc/apk/repositories /etc/apk/keys ./etc/apk/ \
    && apk --root . --initdb --no-cache add \
        alpine-base=~3.13 \
        dhcpcd=~8.1 \
        linux-virt=~5.10 \
    && ln -s /etc/init.d/devfs ./etc/runlevels/sysinit/devfs \
    && ln -s /etc/init.d/dmesg ./etc/runlevels/sysinit/dmesg \
    && ln -s /etc/init.d/mdev ./etc/runlevels/sysinit/mdev \
    && ln -s /etc/init.d/hwdrivers ./etc/runlevels/sysinit/hwdrivers \
    && ln -s /etc/init.d/cgroups ./etc/runlevels/sysinit/cgroups \
    && ln -s /etc/init.d/modules ./etc/runlevels/boot/modules \
    && ln -s /etc/init.d/hwclock ./etc/runlevels/boot/hwclock \
    && ln -s /etc/init.d/swap ./etc/runlevels/boot/swap \
    && ln -s /etc/init.d/hostname ./etc/runlevels/boot/hostname \
    && ln -s /etc/init.d/sysctl ./etc/runlevels/boot/sysctl \
    && ln -s /etc/init.d/bootmisc ./etc/runlevels/boot/bootmisc \
    && ln -s /etc/init.d/syslog ./etc/runlevels/boot/syslog \
    && ln -s /etc/init.d/networking ./etc/runlevels/boot/networking

COPY fstab ./etc/fstab
COPY interfaces ./etc/network/interfaces
COPY init ./init

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN find . -print0 | cpio --null -ov --format=newc | gzip -9 > /initrd.img

FROM docker.io/library/alpine:3.13.2@sha256:4661fb57f7890b9145907a1fe2555091d333ff3d28db86c3bb906f6a2be93c87

RUN addgroup -S build -g 1000 && adduser -S build -G build -u 1000

RUN apk --no-cache add \
    linux-virt=~5.10 \
    qemu-system-x86_64=~5.2

COPY --from=initrd /initrd.img /initrd.img

USER build
