# QEMU Builder

Build inside a QEMU VM inside a container

This is useful for building inside unprivileged non-root containers, at a significant performance penalty.

When booted the provided initial ramdisk will run build.sh in the host mounted path.

Example usage (in container):

```sh
PROJECT_PATH=/path/to/project
qemu-system-x86_64 \
    -m 1G \
    -nographic \
    -kernel /boot/vmlinuz-virt \
    -initrd /initrd.img \
    -append "console=ttyS0" \
    -virtfs "local,path=$PROJECT_PATH,mount_tag=host,security_model=mapped-xattr"
```
