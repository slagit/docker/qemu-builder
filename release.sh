#!/bin/sh

set -eu

VERSION=$1

skopeo copy \
    --dest-creds="$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" \
    oci:qemu-builder.oci \
    "docker://$CI_REGISTRY_IMAGE:$VERSION"
